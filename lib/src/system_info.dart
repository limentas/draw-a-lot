class SystemInfo {
  String supportedABIs;
  DateTime buildTime;
  String tags;
  String hardware;
  String device;
  String brand;
  bool isX86_32 = false; //main ABI is x86 (32bit)
}
